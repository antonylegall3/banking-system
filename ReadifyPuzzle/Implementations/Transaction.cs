﻿// Author: Antony Le Gall
// E-Mail: aantlegall@gmail.com
using System;
using ReadifyBank.Interfaces;

/// <summary>
/// Concrete Implementation of the Readify Bank IStatementRow Interface
/// </summary>
public class Transaction : IStatementRow
{
    /// <summary>
    /// Account on which the transaction is made
    /// </summary>
    public IAccount Account { get; }

    /// <summary>
    /// Date and time of the transaction
    /// </summary>
    public DateTimeOffset Date { get; }

    /// <summary>
    /// Amount of the operation
    /// </summary>
    public decimal Amount { get; }

    /// <summary>
    /// Balance of the account after the transaction
    /// </summary>
    public decimal Balance { get; }

    /// <summary>
    /// Description of the transaction
    /// </summary>
    public string Description { get; }

    /// <summary>
    /// A constructor to create a transaction instance
    /// </summary>
    /// <param name="account">Account on which the transaction is made</param>
    /// <param name="date">Date and time of the transaction</param>
    /// <param name="amount">Amount of the operation</param>
    /// <param name="balance">Balance of the account before the transaction</param>
    /// <param name="description">Description of the transaction</param>
    public Transaction(IAccount account, DateTimeOffset date, decimal amount, decimal balance, string description)
	{
        Account = account;
        Date = date;
        Amount = amount;
        Balance = balance;
        Description = description;
	}
}
