﻿// Author: Antony Le Gall
// E-Mail: aantlegall@gmail.com
using ReadifyBank.Interfaces;
using System;
using System.Collections.Generic;

/// <summary>
/// Concrete Implementation of the IReadifyBank Interface
/// </summary>
public class BankController : IReadifyBank
{
    /// <summary>
    /// Bank accounts list
    /// </summary>
    public IList<IAccount> AccountList { get; }

    /// <summary>
    /// Transactions log of the bank
    /// </summary>
    public IList<IStatementRow> TransactionLog { get; }

    /// <summary>
    /// The constructor to Initialize fields
    /// </summary>
    public BankController()
    {
        AccountList = new List<IAccount>();
        TransactionLog = new List<IStatementRow>();
    }

    /// <summary>
    /// Open a home loan account
    /// </summary>
    /// <param name="customerName">Customer name</param>
    /// <param name="openDate">The date of the transaction</param>
    /// <returns>Opened Account</returns>
    public IAccount OpenHomeLoanAccount(string customerName, DateTimeOffset openDate)
    {
        IAccount homeLoanAccount = new HomeLoanAccount(customerName, openDate);
        AccountList.Add(homeLoanAccount);
        return homeLoanAccount;
    }

    /// <summary>
    /// Open a savings account
    /// </summary>
    /// <param name="customerName">Customer name</param>
    /// <param name="openDate">The date of the transaction</param>
    /// <returns>Opened account</returns>
    public IAccount OpenSavingsAccount(string customerName, DateTimeOffset openDate)
    {
        IAccount savingsAccount = new SavingsAccount(customerName, openDate);
        AccountList.Add(savingsAccount);
        return savingsAccount;
    }

    /// <summary>
    /// Deposit amount in an account
    /// </summary>
    /// <param name="account">Account</param>
    /// <param name="amount">Deposit amount</param>
    /// <param name="description">Description of the transaction</param>
    /// <param name="depositDate">The date of the transaction</param>
    public void PerformDeposit(IAccount targetAccount, decimal amount, string description, DateTimeOffset depositDate)
    {
        // Get the balance
        decimal currentBalance = targetAccount.Balance;
        // Ensure the user enters a value above 0
        if (amount <= 0)
        {
            throw new ArgumentException("Please enter a deposit amount above 0");
        }
        else
        {
            // Withdraw the funds and create the transaction to add to the list
            targetAccount.Balance = (currentBalance + amount);
            IStatementRow transaction = new Transaction(targetAccount, depositDate, amount, currentBalance, description);
            TransactionLog.Add(transaction);
        }
    }

    /// <summary>
    /// Withdraw amount in an account
    /// </summary>
    /// <param name="account">Account</param>
    /// <param name="amount">Withdrawal amount</param>
    /// <param name="description">Description of the transaction</param>
    /// <param name="withdrawalDate">The date of the transaction</param>
    public void PerformWithdrawal(IAccount targetAccount, decimal amount, string description, DateTimeOffset withdrawalDate)
    {
        // Get the balance
        decimal currentBalance = targetAccount.Balance;
        // Check if the account has enough funds
        if (currentBalance < amount)
        {
            throw new ArgumentException("The account doesnt have sufficient funds to withdraw");
        }
        // Ensure the user enters a value above 0
        else if (amount <= 0)
        {
            throw new ArgumentException("Please enter a withdrawal amount above 0");
        }
        else
        {
            // Withdraw the funds and create the transaction to add to the list
            targetAccount.Balance = (currentBalance - amount);
            IStatementRow transaction = new Transaction(targetAccount, withdrawalDate, amount, currentBalance, description);
            TransactionLog.Add(transaction);
        }
    }

    /// <summary>
    /// Transfer amount from an account to an account
    /// </summary>
    /// <param name="from">From account</param>
    /// <param name="to">To account</param>
    /// <param name="amount">Transfer amount</param>
    /// <param name="description">Description of the transaction</param>
    /// <param name="transferDate">The date of the transaction</param>
    public void PerformTransfer(IAccount from, IAccount to, decimal amount, string description, DateTimeOffset transferDate)
    {
        try
        {
            PerformWithdrawal(from, amount, description, transferDate);
            PerformDeposit(to, amount, description, transferDate);
        }
        // Catch the exception and inform the user of the failed operation
        catch (ArgumentException e)
        {
            Console.WriteLine(e.Message + "The Transfer has been cancelled");
        }
    }

    /// <summary>
    /// Return the balance for an account
    /// </summary>
    /// <param name="account">Customer account</param>
    /// <returns></returns>
    public decimal GetBalance(IAccount account)
    {
        return account.Balance;
    }

    /// <summary>
    /// Calculate interest rate for an account to a specific time
    /// The interest rate for Saving account is 6% monthly
    /// The interest rate for Home loan account is 3.99% annually
    /// </summary>
    /// <param name="account">Customer account</param>
    /// <param name="toDate">Calculate interest to this date</param>
    /// <returns>The added value</returns>
    public decimal CalculateInterestToDate(IAccount account, DateTimeOffset toDate)
    {
        decimal interestRate, interest = 0.0m;
        int diff;
        // Calculate the days between the account opening and the provided date
        diff = (toDate - account.OpenedDate).Days;
        //Check account type
        if (account is SavingsAccount)
        {
            // Convert the days into months to apply the interest monthly
            diff = diff / 30;
            interestRate = 0.06m;
            // Loop through each month calculating the interest
            for (int i = 0; i < diff; i++)
            {
                interest += interestRate * account.Balance;
            }
        }
        else
        {
            // Convert the days into years to apply the interest annually
            diff = diff / 365;
            interestRate = 0.0399m;
            // Loop through each year calculating the interest
            for (int i = 0; i < diff; i++)
            {
                interest += interestRate * account.Balance;
            }
        }
        return interest;
    }

    /// <summary>
    /// Get mini statement (the last 5 transactions occurred on an account)
    /// </summary>
    /// <param name="account">Customer account</param>
    /// <returns>Last five transactions</returns>
    public IEnumerable<IStatementRow> GetMiniStatement(IAccount account)
    {
        IList<IStatementRow> transactions = new List<IStatementRow>();
        {
            int count = 0, i = TransactionLog.Count;
            // Decrement for 0 indexing
            i--;
            // Loop through the transaction log taking the last 5 statements for the given account
            while((count < 5) && (i >= 0))
            {
                if (TransactionLog[i].Account == account)
                {
                    transactions.Add(TransactionLog[i]);
                    count++;
                }
                i--;
            }
        }
        return transactions;
    }

    /// <summary>
    /// Close an account
    /// </summary>
    /// <param name="account">Customer account</param>
    /// <param name="closeDate">Close Date</param>
    /// <returns>All transactions happened on the closed account</returns>
    public IEnumerable<IStatementRow> CloseAccount(IAccount account, DateTimeOffset closeDate)
    {
        IList<IStatementRow> transactions = new List<IStatementRow>();
        // Loop through the transaction log finding transactions related to the given account
        for(int i = 0; i < TransactionLog.Count; i++)
        {
            if (TransactionLog[i].Account == account)
            {
                transactions.Add(TransactionLog[i]);
            }
        }
        return transactions;
    }
}