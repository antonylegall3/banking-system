﻿// Author: Antony Le Gall
// E-Mail: aantlegall@gmail.com
using System;
using ReadifyBank.Interfaces;

/// <summary>
/// Savings Account Implementation of the Readify Bank IAccount Interface
/// </summary>
public class SavingsAccount : IAccount
{
    /// <summary>
    /// The date when the account was opened
    /// </summary>
    public DateTimeOffset OpenedDate { get; }

    /// <summary>
    /// Customer Name
    /// </summary>
    public string CustomerName { get; }

    /// <summary>
    /// Account number 
    /// </summary>
    public string AccountNumber { get; }

    /// <summary>
    /// Current account balance
    /// </summary>
    public decimal Balance { get; set; }

    /// <summary>
    /// Private unchanging variable specifying the 2 character account type
    /// </summary>
    private const string accountType = "SV";

    /// <summary>
    /// A static counter variable to keep track of how many savings accounts have been opened
    /// </summary>
    private static int Counter = 0;

    /// <summary>
    /// The constructor to open a savings account
    /// </summary>
    /// <param name="customerName">Customer name</param>
    /// <param name="openDate">The date the account opened</param>
    /// <returns>Nothing</returns>
    public SavingsAccount(String customerName, DateTimeOffset openDate)
	{
        OpenedDate = openDate;
        CustomerName = customerName;
        // Insert the account type and number into the string
        AccountNumber = $"{accountType}-{ConvertCounter()}";
        Balance = 0.0m;
	}

    /// <summary>
    /// Converts and Pads the Counter variable
    /// </summary>
    /// <returns>Padded string representing the number of accounts opened</returns>
    public string ConvertCounter()
    {
        // Increase the counter as an account has been opened
        Counter++;
        string paddedString = Counter.ToString();
        return paddedString = paddedString.PadLeft(6, '0');
    }
}
