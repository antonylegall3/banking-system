// Author: Antony Le Gall
// E-Mail: aantlegall@gmail.com
using ReadifyBank.Interfaces;
using System;
using System.Collections.Generic;

namespace ReadifyPuzzle
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initializing
            BankController controller = new BankController();
            IList<IAccount> AccountList = controller.AccountList;
            IEnumerable<IStatementRow> transactions = controller.TransactionLog;
            IAccount targetAccount;
            String name, description;
            decimal amount;
            bool done = false;

            // Loop until the user chooses to exit
            while(!done)
            {
                Console.WriteLine("======Welcome to Readify Bank!======\nPlease Select an option from below by entering the highlighted letter\n\nOpen a (H)ome Loan Account\n" +
                "Open a (S)avings Account\n(D)eposit\n(W)ithdraw\n(T)ransfer\nGet (B)alance\nCalculate (I)nterest\n(G)et a Mini Statement\n(C)lose an Account\n(E)xit");
                char choice = Char.Parse(Console.ReadLine());
                
                // A large switch statement to handle the various different options provided by the interface
                switch (choice)
                {
                    // Open a home loan account
                    case 'H':
                        Console.WriteLine("Please enter the name associated with the account");
                        name = Console.ReadLine();
                        IAccount homeLoanAccount = controller.OpenHomeLoanAccount(name, DateTimeOffset.Now);
                        break;
                    // Open a savings account
                    case 'S':
                        Console.WriteLine("Please enter the name associated with the account");
                        name = Console.ReadLine();
                        IAccount savingsAccount = controller.OpenHomeLoanAccount(name, DateTimeOffset.Now);
                        break;
                    // Deposit funds into an account
                    case 'D':
                        // User input
                        Console.WriteLine("Please enter the name associated with the account");
                        name = Console.ReadLine();
                        // Find the account in the list
                        targetAccount = findAccount(AccountList, name);

                        // If its not in the list notify the user and exit the transaction
                        if (targetAccount == null)
                        {
                            Console.WriteLine("Unable to find an account under that name");
                            break;
                        }
                        // User input
                        Console.WriteLine("Please enter the amount you would like to deposit");
                        amount = Decimal.Parse(Console.ReadLine());
                        Console.WriteLine("Please enter a description for this deposit");
                        description = Console.ReadLine();

                        try
                        {
                            controller.PerformDeposit(targetAccount, amount, description, DateTimeOffset.Now);
                        }
                        // Catch the exception and inform the user of the failed operation
                        catch (ArgumentException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    // Withdraw funds from an account
                    case 'W':
                        // User input
                        Console.WriteLine("Please enter the name associated with the account");
                        name = Console.ReadLine();
                        // Find the account
                        targetAccount = findAccount(AccountList, name);
                        // If its not in the list notify the user and exit the transaction
                        if (targetAccount == null)
                        {
                            Console.WriteLine("Unable to find an account under that name");
                            break;
                        }
                        // User input
                        Console.WriteLine("Please enter the amount you would like to withdraw");
                        amount = Decimal.Parse(Console.ReadLine());
                        Console.WriteLine("Please enter a description for this deposit");
                        description = Console.ReadLine();
                        try
                        {
                            controller.PerformWithdrawal(targetAccount, amount, description, DateTimeOffset.Now);
                        }
                        // Catch the exception and inform the user of the failed operation
                        catch (ArgumentException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    // Transfer funds from an account to another
                    case 'T':
                        IAccount targetAccount2;
                        // User input
                        Console.WriteLine("Please enter the name associated with the account you want to transfer from");
                        name = Console.ReadLine();
                        // Find the account
                        targetAccount = findAccount(AccountList, name);
                        // If its not in the list notify the user and exit the transaction
                        if (targetAccount == null)
                        {
                            Console.WriteLine("Unable to find an account under that name");
                            break;
                        }
                        // Same as process but for another account
                        Console.WriteLine("Please enter the name associated with the account you want to transfer to");
                        name = Console.ReadLine();
                        targetAccount2 = findAccount(AccountList, name);
                        if (targetAccount2 == null)
                        {
                            Console.WriteLine("Unable to find an account under that name");
                            break;
                        }
                        // User input
                        Console.WriteLine("Please enter the amount you would like to transfer");
                        amount = Decimal.Parse(Console.ReadLine());
                        Console.WriteLine("Please enter a description for this transfer");
                        description = Console.ReadLine();
                        controller.PerformTransfer(targetAccount, targetAccount2, amount, description, DateTimeOffset.Now);
                        break;
                    // Get the balance on an account
                    case 'B':
                        Console.WriteLine("Please enter the name associated with the account you want to check the balance of");
                        name = Console.ReadLine();
                        targetAccount = findAccount(AccountList, name);
                        Console.WriteLine($"The Balance for {targetAccount.CustomerName} is {controller.GetBalance(targetAccount)}");
                        break;
                    // Calculate interest on an account to a specific date
                    case 'I':
                        string date;
                        string[] dates;
                        int days, months, years;
                        decimal interest;
                        // Attempt to find the account
                        Console.WriteLine("Please enter the name associated with the account you want to calculate interest on");
                        name = Console.ReadLine();
                        targetAccount = findAccount(AccountList, name);
                        Console.WriteLine("Please enter the date you would like to calculate to in DD/MM/YYYY");
                        // Break down the entered date into days months and years 
                        date = Console.ReadLine();
                        dates = date.Split('/');
                        days = int.Parse(dates[0]);
                        months = int.Parse(dates[1]);
                        years = int.Parse(dates[2]);
                        // Calculate and print the interest accrued on the account
                        interest = controller.CalculateInterestToDate(targetAccount, new DateTimeOffset(years, months, days, 1, 1, 1, new TimeSpan(1, 0, 0)));
                        Console.WriteLine($"Interest on the account {name} is {interest}");
                        break;
                    // Get a mini statement for an account
                    case 'G':
                        // Get the user input and attempt to find the account
                        Console.WriteLine("Please enter the name associated with the account you want to get a statement on");
                        name = Console.ReadLine();
                        targetAccount = findAccount(AccountList, name);
                        if (targetAccount == null)
                        {
                            Console.WriteLine("Unable to find an account under that name");
                            break;
                        }
                        transactions = controller.GetMiniStatement(targetAccount);
                        // Print the transactions neatly
                        Console.WriteLine("Mini Statement");
                        foreach (IStatementRow row in transactions)
                        {
                            Console.WriteLine("Date: " + row.Date + " Amount: " + row.Amount + " Balance: " + row.Balance + " Description: " + row.Description);
                        }
                        break;
                    // Close an account
                    case 'C':
                        // Get the user input and attempt to find the account
                        Console.WriteLine("Please enter the name associated with the account you want to close");
                        name = Console.ReadLine();
                        targetAccount = findAccount(AccountList, name);
                        if (targetAccount == null)
                        {
                            Console.WriteLine("Unable to find an account under that name");
                            break;
                        }
                        // Close the account and print the transactions
                        transactions = controller.CloseAccount(targetAccount, DateTimeOffset.Now);
                        Console.WriteLine("\nAll Transactions that occured on closed account");
                        foreach (IStatementRow row in transactions)
                        {
                            Console.WriteLine("Date: " + row.Date + " Amount: " + row.Amount + " Balance: " + row.Balance + " Description: " + row.Description);
                        }
                        break;
                    // Exit
                    case 'E':
                        done = true;
                        break;
                }
            }
        }

        // Helper function called numerous times to locate an account given a name
        public static IAccount findAccount(IList<IAccount> AccountList, string name)
        {
            IAccount targetAccount = null;
            // Loop through the list looking for the account under the provided name
            for (int i = 0; i < AccountList.Count; i++)
            {
                if (AccountList[i].CustomerName.Equals(name))
                {
                    targetAccount = AccountList[i];
                }
            }
            // Return the account or null if not found
            return targetAccount;
        }
    }
}
